""" Implementation of a DTED tile. """
from dataclasses import astuple
from pathlib import Path
from struct import unpack
from typing import Optional, Union
from warnings import warn

import numpy as np
from scipy.signal import convolve2d

from .definitions import ACC_SIZE, DSI_SIZE, UHL_SIZE, VOID_DATA_VALUE
from .errors import InvalidFileError, NoElevationDataError, VoidDataWarning, NotImplementedError
from .records import AccuracyDescription, DataSetIdentification, UserHeaderLabel

_FilePath = Union[str, Path]
_DATA_SENTINEL = 0xAA


class Tile:
    # noinspection PyUnresolvedReferences
    """An API for accessing data within a DTED file.

    When this class is initialized with a path to a DTED file, the metadata records
      associated with the DTED file will always parsed and the terrain elevation data
      will optionally be parsed (defaults to being parsed).

    By not loading all of the terrain elevation data into memory, you can quickly
      perform elevation lookups on raw files.

    Attributes:
        file: The path to the DTED file.
        uhl: Parsed User Header Label (UHL) record from the DTED file.
        dsi: Parsed Data Set Identification (DSI) record from the DTED file.
        acc: Parsed Accuracy Description (ACC) record from the DTED file.
        data: The raw terrain elevation data from the DTED file.

    Methods:
        get_elevation: Lookup the terrain elevation at a particular location.
            Data does not need to be loaded into memory to perform this operation.
        fill: Fill any void/invlaid regions in the elevation data by using the surrounding
            elevation values. Done on read if infill=True.

    Examples:

        Access metadata within a DTED file.
        >>> from dted import Tile
        >>> tile = Tile("/path/to/DTED.dt2")
        >>> tile.dsi.origin
        LatLon(latitude=41.0, longitude=-71.0)

        Read a DTED file and get the maximum terrain elevation
        >>> from dted import Tile
        >>> dted_file: Path
        >>> tile = Tile(dted_file)
        >>> tile.data.max()
        125

        Lookup terrain elevation at a specific location within a DTED file.
        >>> from dted import LatLon, Tile
        >>> dted_file: Path
        >>> tile = Tile(dted_file)
        >>> tile.get_elevation(lat=41.36, lon=-70.55))
        -21

    References:
        SRTM DTED Specification:
            https://www.dlr.de/eoc/Portaldata/60/Resources/dokumente/7_sat_miss/SRTM-XSAR-DEM-DTED-1.1.pdf
    """

    def __init__(self, file, infill=False):
        """
        Args:
            file: The DTED file to parse.
        """
        self.file = Path(file)

        with self.file.open("rb") as f:
            self.uhl = UserHeaderLabel.from_bytes(f.read(UHL_SIZE))
            self.dsi = DataSetIdentification.from_bytes(f.read(DSI_SIZE))
            self.acc = AccuracyDescription.from_bytes(f.read(ACC_SIZE))

        self.load_data()
        if (infill):
            self.fill_data()
        else:
            self.infill = False

    @property
    def data(self):
        """Access the elevation data."""
        return self._data

    def get_elevation(self, lat, lon, interpolation_type='nearest'):
        """Interpolate the DTED to find the elevation at the specified points"""

        origin_latitude, origin_longitude = astuple(self.dsi.origin)
        longitude_count, latitude_count = self.dsi.shape
        lat_interval, lon_interval = self.dsi.latitude_interval, self.dsi.longitude_interval
        latitude_index = (lat - origin_latitude) * (latitude_count - 1) / lat_interval
        longitude_index = (lon - origin_longitude) * (longitude_count - 1) / lon_interval

        if (interpolation_type.lower() == 'nearest'):
            return np.reshape(self._data[np.rint(longitude_index).astype(int), np.rint(latitude_index).astype(int)],
                              (np.size(lat)))
        elif (interpolation_type.lower() == 'bilinear'):
            # unit square method https://en.wikipedia.org/wiki/Bilinear_interpolation#Alternative_matrix_form
            lon_base = np.floor(longitude_index).astype(int)
            lon_frac = longitude_index - lon_base
            lat_base = np.floor(latitude_index).astype(int)
            lat_frac = latitude_index - lat_base

            neighbor_arr = np.array([[self._data[lon_base, lat_base], self._data[lon_base + 1, lat_base]],
                                    [self._data[lon_base, lat_base + 1], self._data[lon_base + 1, lat_base + 1]]])

            lon_vec = np.array([[1 - lon_frac, lon_frac]])
            lat_vec = np.array([[1 - lat_frac], [lat_frac]])
            # this is a way of representing matrix multiplication but with axis 2 as a "page"
            einsum_str = 'ijl,jkl->ikl'
            if len(np.shape(neighbor_arr)) <= 2:
                # when lat, lon are only scalars, having a third dimension will cause an error
                einsum_str = einsum_str.replace('l','')
            return np.reshape(np.einsum(einsum_str, np.einsum(einsum_str, lon_vec, neighbor_arr), lat_vec), np.shape(lat))
        else:
            raise NotImplementedError(
                f"This interpolation type: {interpolation_type} has not been implemented yet."
            )

    def fill(self):
        """Fill void areas of the DTED with a POCS-like routine"""

        void_idxs = np.argwhere(self._data == VOID_DATA_VALUE)
        #if void cell has no neighboring good cells, will continue to resolve from outside in
        while (void_idxs.size > 0):
            for i in void_idxs:
                val = 0
                count = 0
                #potential for other techniques here
                for j in np.array([[-1, 0], [1, 0], [0, -1], [0, 1]]):
                    try:
                        neighbor = self._data[tuple(i + j)].astype(float)
                        # don't use void cells as data donors
                        if (neighbor != VOID_DATA_VALUE):
                            val += neighbor
                            count += 1
                    finally:
                        pass
                if count > 0:
                    self._data[tuple(i)] = val / count
            void_idxs = np.argwhere(self._data == VOID_DATA_VALUE)

    def load_data(self, perform_checksum= True):
        """Load the elevation data into memory.

        This loaded elevation data can be accessed through the `self.data` attribute.

        Args:
            perform_checksum: Whether to perform the checksum for each data block.
                The user is allowed to toggle this off but it is strongly suggested
                that the checksums be performed.

        Raises:
            InvalidFileError: If a data block fails it's checksum.

        Warnings:
            VoidDataWarning: If void data is detected within the DTED file.
        """

        # Open the file, seek to the data blocks, and start parsing.
        with self.file.open("rb") as f:
            f.seek(UHL_SIZE + DSI_SIZE + ACC_SIZE)
            data_record = f.read()

        block_length = self.dsi.data_block_length
        parsed_data_blocks = [
            _parse_data_block(
                block=data_record[column * block_length : (column + 1) * block_length],
                perform_checksum=perform_checksum,
            )
            for column in range(self.dsi.shape[0])
        ]
        self._data = _convert_signed_magnitude(np.asarray(parsed_data_blocks))

        if VOID_DATA_VALUE in self._data:
            warn(  # Void Data Warning  )
                f"\n\tVoid data has been detected within the DTED file ({self.file})."
                f"\n\tVOID_DATA_VALUE={VOID_DATA_VALUE}",
                category=VoidDataWarning,
            )

    def __str__(self):
        """Output basic information about the DTED tile if a user prints the object"""
        compilation_date = ""
        if self.dsi.compilation_date is not None:
            compilation_date = self.dsi.compilation_date.strftime("%m/%Y")
        maintenance_date = ""
        if self.dsi.maintenance_date is not None:
            maintenance_date = self.dsi.maintenance_date.strftime("%m/%Y")
        nw = self.dsi.north_west_corner.format(1)
        ne = self.dsi.north_east_corner.format(1)
        sw = self.dsi.south_west_corner.format(1)
        se = self.dsi.south_east_corner.format(1)
        resolution = f'{self.dsi.latitude_interval:.1f}"/{self.dsi.longitude_interval:.1f}"'
        accuracy = f"{self.acc.absolute_vertical}m/{self.acc.absolute_horizontal}m"

        # Generate the report.
        return "\n".join(
            [
                f"File Path:          {self.file} ({self.file.stat().st_size >> 20} MB)",
                f"Product Level:      {self.dsi.product_level}",
                f"Security Code:      {self.dsi.security_code}",
                f"Compilation Date:   {compilation_date}",
                f"Maintenance Date:   {maintenance_date}",
                f"Datums (V/H):       {self.dsi.vertical_datum}/{self.dsi.horizontal_datum}",
                f"",
                f"    {nw}      {ne}",
                f"          NW --------------- NE     ",
                f"          |                   |     ",
                f"          |                   |     ",
                f"          |                   |     ",
                f"          |                   |     ",
                f"          |                   |     ",
                f"          |                   |     ",
                f"          SW --------------- SE     ",
                f"    {sw}      {se}",
                f"",
                f"Origin:                 {self.dsi.origin.format(1)}",
                f"Resolution (lat/lon):   {resolution}",
                f"Accuracy (V/H):         {accuracy}",
            ]
        )


def _parse_data_block(block, perform_checksum=False):
    """Parse an individual block of data.

    Args:
        block: A single data block of raw binary data.
        perform_checksum: Whether to perform the checksum verification.

    Returns:
        Parsed terrain elevation data as a numpy array. NOTE: This elevation is data
          is _not_ converted from signed-magnitude representation. This is not done
          in this step to optimize the file parsing.

    Raises:
        InvalidFileError: If the checksum fails verification or the data block is malformed.
    """
    if perform_checksum:
        checksum = unpack(">i", block[-4:])[0]
        sum_ = np.frombuffer(block[:-4], dtype=">B").sum()
        if sum_ != checksum:
            block_index = (_DATA_SENTINEL << 24) - unpack(">I", block[:4])[0]
            raise InvalidFileError(
                f"Checksum failed for data block {block_index}. "
                f"Expected {checksum} -- Found {sum_} "
            )
    if block[0] != _DATA_SENTINEL:
        raise InvalidFileError(
            f"All data blocks within a DTED file must begin with {_DATA_SENTINEL}. "
            f"Found: {block[0]}"
        )

    return np.frombuffer(block[8:-4], dtype=">i2")


def _convert_signed_magnitude(data: np.ndarray) -> np.ndarray:
    """Converts a numpy array of binary 16 bit integers between
    signed magnitude and 2's complement.
    """
    if not data.flags.writeable:
        data = data.copy()
    negatives = data < 0
    data[negatives] = np.array(0x8000, dtype=">i2") - data[negatives]
    return data
