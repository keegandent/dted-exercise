""" Simplified imports for the DTED parsing module. """

from .latlon import LatLon
from .tile import Tile
from .definitions import VOID_DATA_VALUE
__version__ = "1.0"

