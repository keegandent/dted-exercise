import numpy as np
import matplotlib.pyplot as plt
plt.ion()

#there is supplied code to read a DTED file
import dted


# The sample file contains DTED information over part of Northern Virginia.
# It can be read and its basic contents displayed like so
nova = dted.Tile('n38_w078_1arc_v1.dt2')
print(nova)
ne = nova.dsi.north_east_corner.format(1)
sw = nova.dsi.south_west_corner.format(1)
plt.figure(1)
plt.title('DTED Level 2 (Un-Filled)')
plt.imshow(nova.data.T,cmap='gray',origin='lower',
           extent=[nova.dsi.south_west_corner.longitude,nova.dsi.south_east_corner.longitude,
                   nova.dsi.south_east_corner.latitude,nova.dsi.north_east_corner.latitude],
           vmin=np.min(nova.data[nova.data > dted.VOID_DATA_VALUE]),
           vmax=np.mean(nova.data[nova.data > dted.VOID_DATA_VALUE]) + 3.0*np.std(nova.data[nova.data > dted.VOID_DATA_VALUE]))
plt.xlabel('Longitude')
plt.ylabel('Latitude')

# The elevation at a specific location is found with the get_elevation() method.
# However this method simply returns the elevation at the closest point in the
# data file, nearest neighbor interpolation is performed
elev = nova.get_elevation(lat=38.89311, lon=-77.447352)
print('\n\nElevation at the Chantilly office is: %.2f m' % elev)




# EXERCISE 1
# There is a method of the dted.Tile object named get_elevation that uses nearest
# neighbor interpolation to return an elevation for a provided Lat/Lon. Extend this
# function so it returns an elevation that is interpolated from surrounding points
# with bilinear interpolation.
interp_elev = nova.get_elevation(lat=38.89311, lon=-77.447352, interpolation_type='bilinear')
print('Interpolated elevation at the Chantilly office is: %.2f m' % interp_elev)

# EXERCISE 2
# This DTED file contains INVALID/VOID areas marked by a value of dted.VOID_DATA_VALUE.
# Currently, requesting an elevation inside/adjacent to one of these areas will return
# the invalid value. Modify the provided dted.Tile object so void regions are more
# appropriately handled.
invalid_elev = nova.get_elevation(lat=38.2467,lon=-77.291)
print('Elevation at void location before filling is: %.2f m' % invalid_elev)
nova.fill()
plt.figure(2)
plt.title('DTED Level 2 (Filled)')
plt.imshow(nova.data.T,cmap='gray',origin='lower',
           extent=[nova.dsi.south_west_corner.longitude,nova.dsi.south_east_corner.longitude,
                   nova.dsi.south_east_corner.latitude,nova.dsi.north_east_corner.latitude],
           vmin=np.min(nova.data),vmax=np.mean(nova.data) + 3.0*np.std(nova.data))
plt.xlabel('Longitude')
plt.ylabel('Latitude')
invalid_filled_elev = nova.get_elevation(lat=38.2467,lon=-77.291)
print('Elevation at void after filling is: %.2f m' % invalid_filled_elev)

# EXERCISE 3
# Rework the get_elevation function such that it can accept more than a single point.
# Consider how well your implementation scales when a large number of points (10000+) are
# passed as input and bilinear interpolation is used
multi_lat = np.arange(nova.dsi.origin.latitude+0.002,nova.dsi.origin.latitude+1.0,0.00007)
multi_lon = np.arange(nova.dsi.origin.longitude+0.002,nova.dsi.origin.longitude+1.0,0.00007)
try:
    multi_elev = nova.get_elevation(lat=multi_lat,lon=multi_lon,interpolation_type='bilinear')
    print('Elevation at requested locations:\n',multi_elev)
    plt.figure(3,figsize=(14,4))
    plt.subplot(131)
    plt.plot(multi_lat,multi_elev,'bo')
    plt.xlabel('Latitude')
    plt.ylabel('Elevation')
    plt.subplot(132)
    plt.plot(multi_lon,multi_elev,'ro')
    plt.xlabel('Longitude')
    plt.ylabel('Elevation')
    plt.subplot(133)
    plt.imshow(nova.data.T,cmap='gray',origin='lower',
            extent=[nova.dsi.south_west_corner.longitude,nova.dsi.south_east_corner.longitude,
                    nova.dsi.south_east_corner.latitude,nova.dsi.north_east_corner.latitude],
            vmin=np.min(nova.data),vmax=np.mean(nova.data) + 3.0*np.std(nova.data))
    plt.plot(multi_lon,multi_lat,'r--')
    plt.xlabel('Longitude')
    plt.ylabel('Latitude')
    plt.tight_layout()
except:
    print('Error encountered passing multiple positions to get_elevation()')

input("Please press the enter key to exit")
